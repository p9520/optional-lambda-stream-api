import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) throws MyBestException {
        Integer myBestInteger = 123;
        Optional.ofNullable(myBestInteger)
                .ifPresentOrElse(Main::printInteger, Main::printIntegerIsAbsent);

        Integer mySecondBestInteger = Optional.ofNullable(myBestInteger)
                .orElse(0);//если myBestInteger == null, то mySecondBestInteger присвоится значение по умолчанияю = 0

        List<String> strings = new ArrayList<>();
        String myBestString = "string string";

        Optional.ofNullable(myBestString)
                .ifPresent(strings::add);//Ссылка на метод объекта string. В данном случае метод добавления элемента

        Long myBestLong = null;
        //Разкоментируйте, иначе будет ошибка на этой строке
        //Long mySecondBestLong = Optional.ofNullable(myBestLong).orElseThrow(MyBestException::new);//Выкидываем ошибку если объект == null

        strings.stream().forEach(System.out::println);//Превращаем коллекцию в стрим и для каждого элемента применяем метод переданные в forEach()

    }

    public static void printInteger(Integer integerToPrint) {
        System.out.println("Целое число есть и оно равно = " + integerToPrint);
    }

    public static void printIntegerIsAbsent() {
        System.out.println("Целого числа нет");
    }

    public static Integer addOneToInt(Integer integer) {
        return integer++;
    }
}
